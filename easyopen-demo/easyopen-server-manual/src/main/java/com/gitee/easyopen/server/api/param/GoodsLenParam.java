package com.gitee.easyopen.server.api.param;

import com.gitee.easyopen.doc.annotation.ApiDocField;

import javax.validation.constraints.NotBlank;

public class GoodsLenParam {

    @ApiDocField(description = "自定义长度", required = true, maxLength = "64", example = "iphoneX")
    private String goods_name;

    @ApiDocField(description = "备注")
    private String remark;

    @ApiDocField(description = "b")
    private Boolean b;

    @ApiDocField(description = "byte")
    private Byte aByte;

    @ApiDocField(description = "short")
    private short aShort;

    @ApiDocField(description = "i")
    private Integer integer;

    @ApiDocField(description = "l")
    private Long aLong;

    @ApiDocField(description = "f")
    private float aFloat;
    @ApiDocField(description = "double")
    private Double aDouble;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getB() {
        return b;
    }

    public void setB(Boolean b) {
        this.b = b;
    }

    public Byte getaByte() {
        return aByte;
    }

    public void setaByte(Byte aByte) {
        this.aByte = aByte;
    }

    public short getaShort() {
        return aShort;
    }

    public void setaShort(short aShort) {
        this.aShort = aShort;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public Long getaLong() {
        return aLong;
    }

    public void setaLong(Long aLong) {
        this.aLong = aLong;
    }

    public float getaFloat() {
        return aFloat;
    }

    public void setaFloat(float aFloat) {
        this.aFloat = aFloat;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public void setaDouble(Double aDouble) {
        this.aDouble = aDouble;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

}
